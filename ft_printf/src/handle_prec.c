/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_prec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 16:37:41 by cserdean          #+#    #+#             */
/*   Updated: 2017/03/03 16:41:36 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_prec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <cserdean@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 09:12:26 by cserdean          #+#    #+#             */
/*   Updated: 2017/03/03 16:33:56 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	handle_prec(t_opt *opt, char **s)
{
	char	*c;

	if (opt->max_fw > 0)
	{
		if (opt->type == 's' && opt->max_fw < (int)ft_strlen(*s))
		{
			c = ft_strnew(opt->max_fw);
			ft_strncpy(c, *s, opt->max_fw);
			*s = c;
		}
		else if (opt->type != 's')
		{
			while ((int)ft_strlen(*s) < opt->max_fw)
				*s = ft_strjoin("0", *s);
		}
	}
}

void	handle_pad(t_opt *opt, char **s)
{
	char	*pad;

	pad = ft_strnew(1);
	pad[0] = opt->type != 's' && ft_strchr(opt->flags, '0') != 0
		&& ft_strchr(opt->flags, '-') == 0 ? '0' : ' ';
	if (opt->min_fw > 0)
	{	
		if (ft_strchr(opt->flags, '0') != 0)
		{
		if (opt->neg || ft_strchr(opt->flags, ' ') != 0
			|| ft_strchr(opt->flags, '+') != 0)
			opt->min_fw--;
		if (ft_strchr(opt->flags, '#') != 0)
			opt->min_fw = opt->type == 'o' ? opt->min_fw - 1 : opt->min_fw - 2;
		}
		if (ft_strchr(opt->flags, '-') != 0)
		{
			while ((int)ft_strlen(*s) < opt->min_fw)
				*s = ft_strjoin(*s, pad);
		}
		if (opt->type == 's')
		{
			while ((int)ft_strlen(*s) < opt->min_fw)
			{
				*s = ft_strjoin(pad, *s);
			}
		}
	}
}

void	handle_hash(t_opt *opt, char **s)
{
	if (ft_strchr(opt->flags, '#'))
	{
		if (opt->type == 'x' && ft_strcmp(*s, "0") != 0)
			*s = ft_strjoin("0x", *s);
		else if (opt->type == 'X' && ft_strcmp(*s, "0") != 0)
			*s = ft_strjoin("0X", *s);
		else if (opt->type == 'o')
			*s = ft_strjoin("0", *s);
	}
}

void	handle_sign(t_opt *opt, char **s)
{
	if (opt->type == 'd' || opt->type == 'D' || opt->type == 'i')
	{
		if (opt->neg)
			*s = ft_strjoin("-", *s);
		else if (ft_strchr(opt->flags, '+') != 0)
			*s = ft_strjoin("+", *s);
		else if (ft_strchr(opt->flags, ' ') != 0)
			*s = ft_strjoin(" ", *s);
	}
}
