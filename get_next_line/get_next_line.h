/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 20:07:12 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/24 13:39:36 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 32
# include "libft/libft.h"
# include <fcntl.h>
# include <unistd.h>

int				get_next_line(const int fd, char **line);

typedef	struct	s_fd
{
	char		*sl;
	int			fd;
	struct s_fd	*next;
}				t_fd;

#endif
