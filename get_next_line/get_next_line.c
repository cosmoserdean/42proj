/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 17:22:51 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/24 13:30:45 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

void	ft_new_list_item(t_fd **new, int fd)
{
	*new = (t_fd *)malloc(sizeof(t_fd));
	(*new)->fd = fd;
	(*new)->next = NULL;
	(*new)->sl = ft_strnew(0);
}

char	*ft_strman(char **s1, char *s2, int pos, char op)
{
	char *n;

	if (op == 'j')
	{
		n = ft_strjoin(*s1, s2);
		ft_strdel(s1);
		return (n);
	}
	n = *s1;
	n += pos;
	n = ft_strdup(n);
	ft_strdel(s1);
	return (n);
}

int		is_newline(char *c)
{
	int i;

	i = 0;
	while (c[i] != '\0' && c[i] != '\n')
	{
		i++;
	}
	return (c[i] == '\n' ? i : -1);
}

t_fd	*ft_init(int *n_pos, char **buf, int fd, t_fd **list)
{
	t_fd *new;

	*buf = ft_strnew(BUFF_SIZE);
	new = *list;
	if (!new)
	{
		ft_new_list_item(&new, fd);
		*list = new;
	}
	else
	{
		while (new->fd != fd && new->next)
			new = new->next;
		if (new->fd != fd)
		{
			ft_new_list_item(&new->next, fd);
			new = new->next;
		}
	}
	*n_pos = is_newline(new->sl);
	return (new);
}

int		get_next_line(const int fd, char **line)
{
	static t_fd	*list = NULL;
	char		*buf;
	int			n_pos;
	int			nred;
	t_fd		*new;

	if (fd >= 2048 || fd < 0 || line == NULL)
		return (-1);
	nred = 1;
	new = ft_init(&n_pos, &buf, fd, &list);
	while (n_pos == -1 && nred != 0)
	{
		ft_strclr(buf);
		nred = read(fd, buf, BUFF_SIZE);
		if (nred < 0)
			return (-1);
		new->sl = nred != 0 ? ft_strman(&new->sl, buf, 0, 'j') : new->sl;
		n_pos = is_newline(new->sl);
	}
	ft_strdel(&buf);
	nred = ft_strlen(new->sl);
	*line = ft_strsub(new->sl, 0, n_pos == -1 ? ft_strlen(new->sl) : n_pos);
	new->sl = ft_strman(&new->sl, NULL,
			(n_pos == -1 ? ft_strlen(new->sl) : n_pos) + 1, 'd');
	return (!(nred == 0));
}
