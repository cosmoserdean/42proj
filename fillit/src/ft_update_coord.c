/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update_coord.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:53:56 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:53:57 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_update_coord_item(t_etris *list, unsigned char size)
{
	unsigned char	*new_xy;
	unsigned char	i;

	new_xy = (unsigned char*)malloc(sizeof(unsigned char ) * 4);
	i = 0;
	while (i < 4)
	{
		new_xy[i] = list->xy[2 * i + 1] * (size + 1) + list->xy[2 * i];
		i++;
	}
	free(list->xy);
	list->xy = new_xy;
}

void		ft_update_coord(t_etris *list, unsigned char size)
{
	t_etris		*node;

	node = list;
	while (node)
	{
		ft_update_coord_item(node, size);
		node = node->next;
	}
}
