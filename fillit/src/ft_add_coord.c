/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_coord.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:50:24 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:50:25 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_add_coord_item(t_etris *list, unsigned char size)
{
	unsigned char	*new_xy;
	unsigned char	i;

	new_xy = (unsigned char*)malloc(sizeof(unsigned char) * 4);
	i = 0;
	while (i < 4)
	{
		new_xy[i] = list->xy[i] + list->xy[i] / size;
		i++;
	}
	free(list->xy);
	list->xy = new_xy;
}

void		ft_add_coord(t_etris *list, unsigned char size)
{
	t_etris		*node;

	node = list;
	while (node)
	{
		ft_add_coord_item(node, size);
		node = node->next;
	}
}
