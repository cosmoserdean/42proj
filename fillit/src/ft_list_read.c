/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_read.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:52:19 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:52:23 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_etris		*ft_list_read(char *file)
{
	t_etris	*list;
	int		fd;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		ft_msg("error", 1);
	list = NULL;
	ft_tetris_read(&list, fd);
	return (list);
}
