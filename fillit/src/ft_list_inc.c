/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_inc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:51:57 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:51:59 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_coord_square(unsigned char *xy, unsigned char *xy_new)
{
	int i;

	i = 0;
	while (i < 8)
	{
		if (xy[i] != xy_new[i])
			return (0);
		i++;
	}
	return (1);
}

t_etris		*ft_new_item(t_etris *list, unsigned char *xy, char letter)
{
	t_etris		*new_node;
	t_etris		*node;

	new_node = (t_etris*)malloc(sizeof(t_etris));
	new_node->xy = xy;
	new_node->letter = letter;
	new_node->copy = 0;
	node = list;
	while (node)
	{
		if (ft_coord_square(xy, node->xy))
		{
			new_node->copy = node->letter;
			break ;
		}
		node = node->next;
	}
	new_node->next = NULL;
	return (new_node);
}

void		ft_add_item(t_etris **list, unsigned char *xy, char letter)
{
	t_etris		*new_node;
	t_etris		*node;

	new_node = ft_new_item(*list, xy, letter);
	node = *list;
	while (node->next)
		node = node->next;
	node->next = new_node;
}

void		ft_list_inc(t_etris **list, unsigned char *xy, char letter)
{
	if (!(*list))
		*list = ft_new_item(*list, xy, letter);
	else
		ft_add_item(list, xy, letter);
}
