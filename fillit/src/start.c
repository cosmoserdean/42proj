/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/15 15:05:23 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:59:44 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

unsigned char	ft_measure_map(t_etris *list)
{
	unsigned char	nr;
	unsigned char	size;
	t_etris			*node;

	nr = 0;
	node = list;
	while (node)
	{
		nr++;
		node = node->next;
	}
	size = 0;
	while (size * size < nr * 4)
		size++;
	return (size);
}

char			*ft_gen_map(unsigned char size)
{
	char	*map;
	int		i;

	map = (char*)malloc(sizeof(char) * ((size + 1) * (size + 1)));
	i = 0;
	while (i < size)
	{
		ft_memset(map + i * (size + 1), '.', size);
		map[(i + 1) * (size + 1) - 1] = '\n';
		i++;
	}
	ft_memset(map + i * (size + 1), '\0', size + 1);
	return (map);
}

void			start(t_etris *list)
{
	char			*map;
	unsigned char	size;

	size = ft_measure_map(list);
	ft_update_coord(list, size);
	while (1)
	{
		map = ft_gen_map(size);
		calc(list, map);
		size++;
		ft_add_coord(list, size);
	}
}
