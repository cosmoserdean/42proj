/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tetris_read.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:53:32 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:53:33 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_tetris_read(t_etris **list, int fd)
{
	unsigned char	*xy;
	char			**piece;
	char			*line;
	char			letter;
	int				loop;

	loop = 1;
	letter = 'A';
	while (loop == 1 && letter <= 'Z')
	{
		piece = ft_piece_read(fd);
		xy = ft_coord_read(piece);
		ft_up_coord(xy);
		ft_list_inc(list, xy, letter);
		if (ft_get_next_line(fd, &line) == 0)
			loop = 0;
		else if (!ft_strequ(line, ""))
			ft_msg("error", 1);
		letter++;
	}
	if (ft_get_next_line(fd, &line) > 0)
		ft_msg("error", 1);
}
