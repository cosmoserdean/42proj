/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_up_coord.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:53:41 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:53:43 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_invalix_xy(unsigned char *xy)
{
	int i;

	i = 0;
	while (i < 8)
	{
		if (xy[i] == 0)
			return (0);
		i += 2;
	}
	return (1);
}

void	ft_update(unsigned char *xy)
{
	int i;

	while (ft_invalix_xy(xy))
	{
		i = 0;
		while (i < 8)
		{
			xy[i]--;
			i += 2;
		}
	}
}

void	ft_up_coord(unsigned char *xy)
{
	ft_update(xy);
	ft_update(xy + 1);
}
