/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:54:07 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:54:08 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int argc, char **argv)
{
	if (argc == 2)
		start(ft_list_read(argv[1]));
	else
		ft_msg("usage: ./fillit [input_files]", 1);
	return (0);
}
