/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:54:20 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:54:21 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				ft_set_piece(t_etris *lst, char *map, unsigned char i)
{
	if ((map[i + lst->xy[0]] != '.') || (map[i + lst->xy[1]] != '.') ||
		(map[i + lst->xy[2]] != '.') || (map[i + lst->xy[3]]) != '.')
		return (0);
	map[i + lst->xy[0]] = lst->letter;
	map[i + lst->xy[1]] = lst->letter;
	map[i + lst->xy[2]] = lst->letter;
	map[i + lst->xy[3]] = lst->letter;
	calc(lst->next, map);
	return (1);
}

void			ft_del_piece(t_etris *lst, char *map, unsigned char i)
{
	map[i + lst->xy[0]] = '.';
	map[i + lst->xy[1]] = '.';
	map[i + lst->xy[2]] = '.';
	map[i + lst->xy[3]] = '.';
}

void			calc(t_etris *lst, char *map)
{
	unsigned char	i;
	char			copy;

	if (!lst)
		ft_show_map(map, 1);
	i = 0;
	copy = lst->copy;
	if (copy != '\0')
	{
		while (map[i] != copy)
			i++;
		i++;
	}
	while (map[i] != '\0')
	{
		if (ft_set_piece(lst, map, i))
			ft_del_piece(lst, map, i);
		i++;
	}
}
