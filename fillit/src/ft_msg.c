/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_msg.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:52:56 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:52:58 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_msg(char *msg, int fd)
{
	ft_putendl_fd(msg, fd);
	exit(0);
}

void	ft_show_map(char *str, int fd)
{
	ft_putstr_fd(str, fd);
	exit(0);
}
