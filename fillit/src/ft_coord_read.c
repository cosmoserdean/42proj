/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_coord_read.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:50:43 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:50:45 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			ft_save_coord(unsigned char *xy, int row, int col)
{
	int i;

	i = 0;
	while (xy[i] != 4)
		i++;
	if (i >= 8)
		ft_msg("error", 1);
	xy[i] = row;
	xy[i + 1] = col;
}

void			ft_get_coord_row(char **piece, int row, unsigned char *xy,
								int *neighbours)
{
	int col;

	col = 0;
	while (col < 4)
	{
		if (piece[row][col] != '.' && piece[row][col] != '#')
			ft_msg("error", 1);
		if (piece[row][col] == '#')
		{
			if (xy[7] != 4)
				ft_msg("error", 1);
			ft_save_coord(xy, col, row);
			if (col != 3 && piece[row][col + 1] == '#')
				(*neighbours)++;
			if (row != 3 && piece[row + 1][col] == '#')
				(*neighbours)++;
		}
		col++;
	}
	if (piece[row][col] != '\0')
		ft_msg("error", 1);
}

unsigned char	*ft_coord_read(char **piece)
{
	unsigned char	*xy;
	int				neighbours;
	int				row;

	xy = (unsigned char*)malloc(sizeof(char) * 8);
	ft_memset(xy, 4, 8);
	neighbours = 0;
	row = 0;
	while (row < 4)
	{
		ft_get_coord_row(piece, row, xy, &neighbours);
		row++;
	}
	if (neighbours < 3)
		ft_msg("error", 1);
	return (xy);
}
