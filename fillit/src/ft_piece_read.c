/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_piece_read.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 15:53:06 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/22 15:53:23 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char			**ft_piece_read(int fd)
{
	char	**piece;
	int		index;

	piece = (char **)malloc(sizeof(char*) * 4);
	index = 0;
	while (index < 4)
	{
		if (ft_get_next_line(fd, piece + index) < 1)
			ft_msg("error", 1);
		index++;
	}
	return (piece);
}
