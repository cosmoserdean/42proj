/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cserdean <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/15 14:46:54 by cserdean          #+#    #+#             */
/*   Updated: 2017/02/19 20:57:07 by cserdean         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H

# define FILLIT_H
# include "libft.h"
# include <fcntl.h>
# define BUF_SIZE 2048

typedef	struct		s_etris
{
	unsigned char	*xy;
	char			letter;
	char			copy;
	struct s_etris	*next;
}					t_etris;
typedef struct		s_file
{
	char			*s;
	int				fd;
	struct s_file	*next;
}					t_file;
t_etris				*ft_list_read(char *file);
void				ft_tetris_read(t_etris **list, int fd);
char				**ft_piece_read(int fd);
void				ft_list_inc(t_etris **list, unsigned char *xy,
						char letter);
void				start(t_etris *list);
void				calc(t_etris *list, char *map);
void				ft_msg(char *msg, int fd);
void				ft_show_map(char *msg, int fd);
unsigned char		ft_mesaure_map(t_etris *list);
unsigned char		*ft_coord_read(char **piece);
void				ft_up_coord(unsigned char *coordinates);
void				ft_update_coord(t_etris *list, unsigned char size);
void				ft_add_coord(t_etris *list, unsigned char size);
char				*ft_gen_map(unsigned char size);
void				ft_add_coord(t_etris *list, unsigned char size);
int					ft_get_next_line(const int fd, char **line);

#endif
